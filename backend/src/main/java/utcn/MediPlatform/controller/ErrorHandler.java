package utcn.MediPlatform.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import utcn.MediPlatform.dto.ExceptionDTO;
import utcn.MediPlatform.exception.EmailAlreadyExistsException;
import utcn.MediPlatform.exception.InvalidRequestContent;
import utcn.MediPlatform.exception.ObjectAlreadyExistsException;
import utcn.MediPlatform.exception.ObjectNotFoundException;

@Component
@RestControllerAdvice
public class ErrorHandler {

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(ObjectAlreadyExistsException.class)
    public ExceptionDTO handlerObjectAlreadyExistsException(ObjectAlreadyExistsException exception) {
        return new ExceptionDTO(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(EmailAlreadyExistsException.class)
    public ExceptionDTO handlerEmailAlreadyExistsException(EmailAlreadyExistsException exception) {
        return new ExceptionDTO(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ObjectNotFoundException.class)
    public ExceptionDTO handlerObjectNotFoundException(ObjectNotFoundException exception) {
        return new ExceptionDTO(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InvalidRequestContent.class)
    public ExceptionDTO handlerInvalidRequestContent(InvalidRequestContent exception) {
        return new ExceptionDTO(exception.getMessage());
    }

}
