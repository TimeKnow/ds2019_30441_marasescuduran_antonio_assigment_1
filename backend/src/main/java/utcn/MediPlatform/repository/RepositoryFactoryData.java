package utcn.MediPlatform.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RepositoryFactoryData implements RepositoryFactory {
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationRepository medicationRepository;
    private final MediUserRepository userRepository;

    @Override
    public MedicationPlanRepository createMedicationPlanRepository() {
        return medicationPlanRepository;
    }

    @Override
    public MedicationRepository createMedicationRepository() {
        return medicationRepository;
    }

    @Override
    public MediUserRepository createUserRepository() {
        return userRepository;
    }
}
