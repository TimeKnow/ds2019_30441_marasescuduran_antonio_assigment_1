package utcn.MediPlatform.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "medication")
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer medicationId;
    private String name;
    private String sideEffects;
    private String dosage;

    @ManyToMany(mappedBy = "medicationSet")
    private Set<MedicationPlan> medicationPlanSet = new HashSet<>();

    public Medication(Integer medicationId) {
        this.medicationId = medicationId;
    }

    public Medication(Integer medicationId, String name, String sideEffects, String dosage) {
        this.medicationId = medicationId;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }
}
