export class MedicationPlan {
  id?: number;
  doctorId: number;
  patientId: number;
  intakeIntervals: string;
  period: string;
  medicationListIds: number[];
}
