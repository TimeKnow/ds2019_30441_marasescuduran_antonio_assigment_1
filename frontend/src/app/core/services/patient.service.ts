import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Patient} from '../models/patient';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    withCredentials: true
  };

  constructor(private http: HttpClient) {
  }

  getAllPatients(): Observable<Patient[]> {
    return this.http.get<Patient[]>('api/patients', this.httpOptions);
  }

  getPatientById(id: number): Observable<Patient> {
    return this.http.get<Patient>('api/patients/' + id, this.httpOptions);
  }

  deletePatient(id: number): Observable<{}> {
    return this.http.delete('api/patients/' + id, this.httpOptions);
  }

  updatePatient(id: number, patient: Patient): Observable<Patient> {
    return this.http.put<Patient>('api/patients/' + id, patient, this.httpOptions);
  }

  createPatient(patient: Patient): Observable<Patient> {
    return this.http.post<Patient>('api/patients', patient, this.httpOptions);
  }
}
