import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Medication} from '../models/medication';

@Injectable({
  providedIn: 'root'
})
export class MedicationService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    withCredentials: true
  };

  constructor(private http: HttpClient) {
  }

  getAllMedications(): Observable<Medication[]> {
    return this.http.get<Medication[]>('api/medications', this.httpOptions);
  }

  getMedicationById(id: number): Observable<Medication> {
    return this.http.get<Medication>('api/medications/' + id, this.httpOptions);
  }

  deleteMedication(id: number): Observable<{}> {
    return this.http.delete('api/medications/' + id, this.httpOptions);
  }

  updateMedication(id: number, medication: Medication): Observable<Medication> {
    return this.http.put<Medication>('api/medications/' + id, medication, this.httpOptions);
  }

  createMedication(medication: Medication): Observable<Medication> {
    return this.http.post<Medication>('api/medications', medication, this.httpOptions);
  }
}
