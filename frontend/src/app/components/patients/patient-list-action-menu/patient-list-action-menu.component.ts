import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-patient-list-action-menu',
  templateUrl: './patient-list-action-menu.component.html',
  styleUrls: ['./patient-list-action-menu.component.css']
})
export class PatientListActionMenuComponent {
  @Input() elementId: number;
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
}
