import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-caregiver-form',
  templateUrl: './caregiver-form.component.html',
  styleUrls: ['./caregiver-form.component.css']
})
export class CaregiverFormComponent {
  @Input() form: FormGroup;
  @Input() actionButtonText: string;
  @Output() actionEvent: EventEmitter<any> = new EventEmitter<any>();
}
