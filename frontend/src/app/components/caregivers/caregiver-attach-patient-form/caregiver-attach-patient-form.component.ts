import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Patient} from '../../../core/models/patient';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-caregiver-attach-patient-form',
  templateUrl: './caregiver-attach-patient-form.component.html',
  styleUrls: ['./caregiver-attach-patient-form.component.css']
})
export class CaregiverAttachPatientFormComponent {
  @Input() patientList: Patient[];
  @Input() form: FormGroup;
  @Input() actionButtonText: string;
  @Output() actionEvent: EventEmitter<any> = new EventEmitter<any>();

}
