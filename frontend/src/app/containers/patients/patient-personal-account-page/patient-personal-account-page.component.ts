import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Patient} from '../../../core/models/patient';
import {selectPatientIsLoading, selectPatientStateSelectedPatient} from '../../../store/selectors/patient.selectors';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/state/app.state';
import {GetPatientById} from '../../../store/actions/patient.actions';
import {AuthUser} from '../../../core/models/auth-user';
import {selectCurrentAuthUser} from '../../../store/selectors/auth.selectors';
import {GetCurrentUser} from '../../../store/actions/auth.actions';

@Component({
  selector: 'app-patient-personal-account-page',
  templateUrl: './patient-personal-account-page.component.html',
  styleUrls: ['./patient-personal-account-page.component.css']
})
export class PatientPersonalAccountPageComponent implements OnInit {

  isLoading$: Observable<boolean>;
  selectedPatient$: Observable<Patient>;
  currentUser$: Observable<AuthUser>;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.isLoading$ = this.store.select(selectPatientIsLoading);
    this.selectedPatient$ = this.store.select(selectPatientStateSelectedPatient);
    this.currentUser$ = this.store.select(selectCurrentAuthUser);
    this.store.dispatch(new GetCurrentUser(true));
    this.currentUser$.subscribe(x => this.store.dispatch(new GetPatientById(x.id)));
  }

}
