import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Patient} from '../../../core/models/patient';
import {AppState} from '../../../store/state/app.state';
import {Store} from '@ngrx/store';
import {selectPatientIsLoading, selectPatientList} from '../../../store/selectors/patient.selectors';
import {DeletePatient, GetAllPatients} from '../../../store/actions/patient.actions';

@Component({
  selector: 'app-patient-page',
  templateUrl: './patient-page.component.html',
  styleUrls: ['./patient-page.component.css']
})
export class PatientPageComponent implements OnInit {

  patientList$: Observable<Patient[]>;
  isLoading$: Observable<boolean>;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.store.dispatch(new GetAllPatients());
    this.isLoading$ = this.store.select(selectPatientIsLoading);
    this.patientList$ = this.store.select(selectPatientList);
  }

  onPatientDelete(id: number) {
    this.store.dispatch(new DeletePatient(id));
  }

}
