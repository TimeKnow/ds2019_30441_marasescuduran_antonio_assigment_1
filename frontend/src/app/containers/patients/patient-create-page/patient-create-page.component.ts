import {Component, OnInit} from '@angular/core';
import {Patient} from '../../../core/models/patient';
import {FormControl, FormGroup} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/state/app.state';
import {ActivatedRoute} from '@angular/router';
import {CreatePatient, UpdatePatient} from '../../../store/actions/patient.actions';

@Component({
  selector: 'app-patient-create-page',
  templateUrl: './patient-create-page.component.html',
  styleUrls: ['./patient-create-page.component.css']
})
export class PatientCreatePageComponent implements OnInit {
  form: FormGroup;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(''),
      name: new FormControl(''),
      birthDate: new FormControl(''),
      gender: new FormControl(''),
      address: new FormControl(''),
      medicalRecord: new FormControl(''),
    });
  }

  onCreate() {
    if (this.form.valid) {
      const newPatient = new Patient();
      newPatient.email = this.form.get('email').value;
      newPatient.name = this.form.get('name').value;
      newPatient.birthDate = this.form.get('birthDate').value.toLocaleString();
      newPatient.gender = this.form.get('gender').value;
      newPatient.address = this.form.get('address').value;
      newPatient.medicalRecord = this.form.get('medicalRecord').value;
      this.store.dispatch(new CreatePatient(newPatient));
    }
  }

}
