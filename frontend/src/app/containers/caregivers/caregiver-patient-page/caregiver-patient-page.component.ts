import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Patient} from '../../../core/models/patient';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/state/app.state';
import {selectCaregiverStateIsLoading, selectedCaregiverStateCaregiverPatientList} from '../../../store/selectors/caregiver.selectors';
import {GetAllPatientsOfCaregivers} from '../../../store/actions/caregiver.actions';
import {AuthUser} from '../../../core/models/auth-user';
import {selectCurrentAuthUser} from '../../../store/selectors/auth.selectors';

@Component({
  selector: 'app-caregiver-patient-page',
  templateUrl: './caregiver-patient-page.component.html',
  styleUrls: ['./caregiver-patient-page.component.css']
})
export class CaregiverPatientPageComponent implements OnInit {

  patientList$: Observable<Patient[]>;
  isLoading$: Observable<boolean>;
  currentUser$: Observable<AuthUser>;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.currentUser$ = this.store.select(selectCurrentAuthUser);
    this.patientList$ = this.store.select(selectedCaregiverStateCaregiverPatientList);
    this.isLoading$ = this.store.select(selectCaregiverStateIsLoading);
    this.currentUser$.subscribe(x => {
      this.store.dispatch(new GetAllPatientsOfCaregivers(x.id));
    });
  }

}
