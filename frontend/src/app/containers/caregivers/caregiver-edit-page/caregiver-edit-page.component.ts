import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Patient} from '../../../core/models/patient';
import {FormControl, FormGroup} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/state/app.state';
import {ActivatedRoute} from '@angular/router';
import {Caregiver} from '../../../core/models/caregiver';
import {GetCaregiverById, UpdateCaregiver} from '../../../store/actions/caregiver.actions';
import {selectCaregiverStateIsLoading, selectedCaregiverStateSelectedCaregiver} from '../../../store/selectors/caregiver.selectors';

@Component({
  selector: 'app-caregiver-edit-page',
  templateUrl: './caregiver-edit-page.component.html',
  styleUrls: ['./caregiver-edit-page.component.css']
})
export class CaregiverEditPageComponent implements OnInit {
  currentId: number;
  isLoading$: Observable<boolean>;
  selectedCaregiver$: Observable<Caregiver>;
  form: FormGroup;

  constructor(private store: Store<AppState>, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.currentId = parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 10);
    this.store.dispatch(new GetCaregiverById(this.currentId));
    this.isLoading$ = this.store.select(selectCaregiverStateIsLoading);
    this.selectedCaregiver$ = this.store.select(selectedCaregiverStateSelectedCaregiver);
    this.form = new FormGroup({
      email: new FormControl(''),
      name: new FormControl(''),
      birthDate: new FormControl(''),
      gender: new FormControl(''),
      address: new FormControl(''),
    });
    this.selectedCaregiver$.subscribe(caregiver => this.setCaregiverFormContent(caregiver));
  }

  setCaregiverFormContent(caregiver: Caregiver) {
    if (!caregiver) {
      return;
    }
    const dateFormatted = caregiver.birthDate.split(' ').length > 1 ? caregiver.birthDate : caregiver.birthDate + ', 00:00:00';
    this.form.get('email').setValue(caregiver.email);
    this.form.get('name').setValue(caregiver.name);
    this.form.get('birthDate').setValue(new Date(dateFormatted));
    this.form.get('gender').setValue(caregiver.gender);
    this.form.get('address').setValue(caregiver.address);
  }

  getCaregiverFromFormContent(): Caregiver {
    const caregiver = new Caregiver();
    caregiver.email = this.form.get('email').value;
    caregiver.name = this.form.get('name').value;
    caregiver.birthDate = this.form.get('birthDate').value.toLocaleString();
    caregiver.gender = this.form.get('gender').value;
    caregiver.address = this.form.get('address').value;
    return caregiver;
  }

  onSave() {
    if (this.form.valid) {
      this.store.dispatch(new UpdateCaregiver(this.currentId, this.getCaregiverFromFormContent()));
    }
  }
}
