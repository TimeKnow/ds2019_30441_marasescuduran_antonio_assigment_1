import {AuthUser} from '../../core/models/auth-user';
import {Patient} from '../../core/models/patient';

export interface PatientState {
  patientList: Patient[];
  isLoading: boolean;
  selectedPatient: Patient,
  errorMessage: string | null;
}

export const initialPatientState: PatientState = {
  patientList: [],
  selectedPatient: null,
  isLoading: false,
  errorMessage: null
};
