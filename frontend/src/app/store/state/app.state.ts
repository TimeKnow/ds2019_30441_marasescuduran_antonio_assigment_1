import {AuthState, initialAuthState} from './auth.state';
import {initialPatientState, PatientState} from './patient.state';
import {CaregiverState, initialCaregiverState} from './caregiver.state';
import {initialMedicationState, MedicationState} from './medication.state';
import {initialMedicationPlanState, MedicationPlanState} from './medication-plan.state';

export interface AppState {
  authState: AuthState;
  patientState: PatientState;
  caregiverState: CaregiverState;
  medicationState: MedicationState;
  medicationPlanState: MedicationPlanState;
}

const initialAppState: AppState = {
  authState: initialAuthState,
  patientState: initialPatientState,
  caregiverState: initialCaregiverState,
  medicationState: initialMedicationState,
  medicationPlanState: initialMedicationPlanState,
};
