import {Caregiver} from '../../core/models/caregiver';
import {Patient} from '../../core/models/patient';

export interface CaregiverState {
  caregiverList: Caregiver[];
  caregiverPatientList: Patient[];
  selectedCaregiver: Caregiver,
  isLoading: boolean;
  errorMessage: string | null;
}

export const initialCaregiverState: CaregiverState = {
  caregiverList: [],
  caregiverPatientList: [],
  selectedCaregiver: null,
  isLoading: false,
  errorMessage: null
};
