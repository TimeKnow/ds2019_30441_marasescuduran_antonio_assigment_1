import {Medication} from '../../core/models/medication';

export interface MedicationState {
  medicationList: Medication[];
  selectedMedication: Medication;
  isLoading: boolean;
  errorMessage: string | null;
}

export const initialMedicationState: MedicationState = {
  medicationList: [],
  selectedMedication: null,
  isLoading: false,
  errorMessage: null
};
