import {MedicationActions, MedicationActionsTypes} from '../actions/medication.actions';
import {initialMedicationState, MedicationState} from '../state/medication.state';

export function medicationReducers(state: MedicationState = initialMedicationState, action: MedicationActions) {
  switch (action.type) {
    case MedicationActionsTypes.GetAllMedications:
      return {
        ...state,
        isLoading: true,
      };
    case MedicationActionsTypes.GetAllMedicationsSuccess:
      return {
        ...state,
        isLoading: false,
        medicationList: action.payload
      };
    case MedicationActionsTypes.GetAllMedicationsFailure:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.payload
      };
    case MedicationActionsTypes.GetMedicationById:
      return {
        ...state,
        isLoading: true
      };
    case MedicationActionsTypes.GetMedicationByIdSuccess:
      return {
        ...state,
        isLoading: false,
        selectedMedication: action.payload
      };
    case MedicationActionsTypes.GetMedicationByIdFailure:
      return {
        ...state,
        selectedMedication: null,
        isLoading: false,
        errorMessage: action.payload
      };
    case MedicationActionsTypes.DeleteMedication:
      return {
        ...state,
        isLoading: true,
        medicationList: state.medicationList.filter(x => x.id !== action.payload)
      };
    case MedicationActionsTypes.DeleteMedicationSuccess:
      return {
        ...state,
        isLoading: false,
      };
    case MedicationActionsTypes.DeleteMedicationFailure:
      return {
        ...state,
        errorMessage: action.payload,
        isLoading: false,
      };
    case MedicationActionsTypes.UpdateMedication:
      return {
        ...state,
        isLoading: true,
      };
    case MedicationActionsTypes.UpdateMedicationSuccess:
      const stateMedicationList = state.medicationList;
      stateMedicationList[action.id] = action.payload;
      return {
        ...state,
        isLoading: false,
        medicationList: stateMedicationList
      };
    case MedicationActionsTypes.UpdateMedicationFailure:
      return {
        ...state,
        errorMessage: action.payload,
        isLoading: false,
      };
    case MedicationActionsTypes.CreateMedication:
      return {
        ...state,
        isLoading: true,
      };
    case MedicationActionsTypes.CreateMedicationSuccess:
      return {
        ...state,
        isLoading: false,
        medicationList: state.medicationList.concat([action.payload])
      };
    case MedicationActionsTypes.CreateMedicationFailure:
      return {
        ...state,
        errorMessage: action.payload,
        isLoading: false,
      };
    default:
      return state;
  }
}
