import {Action} from '@ngrx/store';
import {Medication} from '../../core/models/medication';

export enum MedicationActionsTypes {
  GetAllMedications = '[Medication] Get All',
  GetAllMedicationsSuccess = '[Medication] Get All Success',
  GetAllMedicationsFailure = '[Medication] Get All Failure',
  GetMedicationById = '[Medication] Get By Id',
  GetMedicationByIdSuccess = '[Medication] Get By Id Success',
  GetMedicationByIdFailure = '[Medication] Get By Id Failure',
  DeleteMedication = '[Medication] Delete',
  DeleteMedicationSuccess = '[Medication] Delete Success',
  DeleteMedicationFailure = '[Medication] Delete Failure',
  UpdateMedication = '[Medication] Update',
  UpdateMedicationSuccess = '[Medication] Update Success',
  UpdateMedicationFailure = '[Medication] Update Failure',
  CreateMedication = '[Medication] Create',
  CreateMedicationSuccess = '[Medication] Create Success',
  CreateMedicationFailure = '[Medication] Create Failure',
}

export class GetAllMedications implements Action {
  public readonly type = MedicationActionsTypes.GetAllMedications;

  constructor() {
  }
}

export class GetAllMedicationsSuccess implements Action {
  public readonly type = MedicationActionsTypes.GetAllMedicationsSuccess;

  constructor(public payload: Medication[]) {
  }
}

export class GetAllMedicationsFailure implements Action {
  public readonly type = MedicationActionsTypes.GetAllMedicationsFailure;

  constructor(public payload: any) {
  }
}

export class GetMedicationById implements Action {
  public readonly type = MedicationActionsTypes.GetMedicationById;

  constructor(public payload: number) {
  }
}

export class GetMedicationByIdSuccess implements Action {
  public readonly type = MedicationActionsTypes.GetMedicationByIdSuccess;

  constructor(public payload: Medication) {
  }
}

export class GetMedicationByIdFailure implements Action {
  public readonly type = MedicationActionsTypes.GetMedicationByIdFailure;

  constructor(public payload: any) {
  }
}

export class DeleteMedication implements Action {
  public readonly type = MedicationActionsTypes.DeleteMedication;

  constructor(public payload: number) {
  }
}

export class DeleteMedicationSuccess implements Action {
  public readonly type = MedicationActionsTypes.DeleteMedicationSuccess;

}

export class DeleteMedicationFailure implements Action {
  public readonly type = MedicationActionsTypes.DeleteMedicationFailure;

  constructor(public payload: any) {
  }
}

export class UpdateMedication implements Action {
  public readonly type = MedicationActionsTypes.UpdateMedication;

  constructor(public id: number, public payload: Medication) {
  }
}

export class UpdateMedicationSuccess implements Action {
  public readonly type = MedicationActionsTypes.UpdateMedicationSuccess;

  constructor(public id: number, public payload: Medication) {
  }

}

export class UpdateMedicationFailure implements Action {
  public readonly type = MedicationActionsTypes.UpdateMedicationFailure;

  constructor(public payload: any) {
  }
}

export class CreateMedication implements Action {
  public readonly type = MedicationActionsTypes.CreateMedication;

  constructor(public payload: Medication) {
  }
}

export class CreateMedicationSuccess implements Action {
  public readonly type = MedicationActionsTypes.CreateMedicationSuccess;

  constructor(public payload: Medication) {
  }
}

export class CreateMedicationFailure implements Action {
  public readonly type = MedicationActionsTypes.CreateMedicationFailure;

  constructor(public payload: any) {
  }
}

export type MedicationActions =
  GetAllMedications
  | GetAllMedicationsSuccess
  | GetAllMedicationsFailure
  | GetMedicationById
  | GetMedicationByIdSuccess
  | GetMedicationByIdFailure
  | DeleteMedication
  | DeleteMedicationSuccess
  | DeleteMedicationFailure
  | UpdateMedication
  | UpdateMedicationSuccess
  | UpdateMedicationFailure
  | CreateMedication
  | CreateMedicationSuccess
  | CreateMedicationFailure;
