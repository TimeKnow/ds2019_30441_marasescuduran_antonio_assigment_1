import {Action} from '@ngrx/store';
import {MedicationPlan} from '../../core/models/medication-plan';

export enum MedicationPlanActionsTypes {
  GetAllMedicationPlans = '[Medication-Plan] Get All',
  GetAllMedicationPlansSuccess = '[Medication-Plan] Get All Success',
  GetAllMedicationPlansFailure = '[Medication-Plan] Get All Failure',
  GetAllMedicationPlansByPatient = '[Medication-Plan] Get All By Patient',
  GetAllMedicationPlansByPatientSuccess = '[Medication-Plan] Get All Success By Patient',
  GetAllMedicationPlansByPatientFailure = '[Medication-Plan] Get All Failure By Patient',
  CreateMedicationPlan = '[Medication-Plan] Create',
  CreateMedicationPlanSuccess = '[Medication-Plan] Create Success',
  CreateMedicationPlanFailure = '[Medication-Plan] Create Failure',
}

export class GetAllMedicationPlans implements Action {
  public readonly type = MedicationPlanActionsTypes.GetAllMedicationPlans;

  constructor() {
  }
}

export class GetAllMedicationPlansSuccess implements Action {
  public readonly type = MedicationPlanActionsTypes.GetAllMedicationPlansSuccess;

  constructor(public payload: MedicationPlan[]) {
  }
}

export class GetAllMedicationPlansFailure implements Action {
  public readonly type = MedicationPlanActionsTypes.GetAllMedicationPlansFailure;

  constructor(public payload: any) {
  }
}

export class GetAllMedicationPlansByPatient implements Action {
  public readonly type = MedicationPlanActionsTypes.GetAllMedicationPlansByPatient;

  constructor(public payload: number) {
  }
}

export class GetAllMedicationPlansByPatientSuccess implements Action {
  public readonly type = MedicationPlanActionsTypes.GetAllMedicationPlansByPatientSuccess;

  constructor(public payload: MedicationPlan[]) {
  }
}

export class GetAllMedicationPlansByPatientFailure implements Action {
  public readonly type = MedicationPlanActionsTypes.GetAllMedicationPlansByPatientFailure;

  constructor(public payload: any) {
  }
}

export class CreateMedicationPlan implements Action {
  public readonly type = MedicationPlanActionsTypes.CreateMedicationPlan;

  constructor(public payload: MedicationPlan) {
  }
}

export class CreateMedicationPlanSuccess implements Action {
  public readonly type = MedicationPlanActionsTypes.CreateMedicationPlanSuccess;

  constructor(public payload: MedicationPlan) {
  }
}

export class CreateMedicationPlanFailure implements Action {
  public readonly type = MedicationPlanActionsTypes.CreateMedicationPlanFailure;

  constructor(public payload: any) {
  }
}

export type MedicationPlanActions =
  GetAllMedicationPlans
  | GetAllMedicationPlansSuccess
  | GetAllMedicationPlansByPatient
  | GetAllMedicationPlansByPatientSuccess
  | GetAllMedicationPlansByPatientFailure
  | GetAllMedicationPlansFailure
  | CreateMedicationPlan
  | CreateMedicationPlanSuccess
  | CreateMedicationPlanFailure;
