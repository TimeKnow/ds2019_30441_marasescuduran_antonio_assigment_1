import {Action} from '@ngrx/store';
import {EmailCredential} from '../../core/models/email-credential';
import {AuthUser} from '../../core/models/auth-user';

export enum AuthActionsTypes {
  GetCurrentUser = '[Auth-User] Get Current',
  GetCurrentUserSuccess = '[Auth-User] Get Current Success',
  LoginUser = '[Auth-User] Login',
  LoginUserSuccess = '[Auth-User] Login Success',
  LoginUserFailure = '[Auth-User] Login Failure',
  LoginRedirect = '[Auth-User] LoginRedirect',
  LogoutUser = '[Auth-User] Logout',
}

export class GetCurrentUser implements Action {
  public readonly type = AuthActionsTypes.GetCurrentUser;

  constructor(public payload: boolean) {
  }
}

export class GetCurrentUserSuccess implements Action {
  public readonly type = AuthActionsTypes.GetCurrentUserSuccess;

  constructor(public payload: AuthUser) {
  }
}

export class LoginUser implements Action {
  public readonly type = AuthActionsTypes.LoginUser;

  constructor(public payload: EmailCredential) {
  }
}

export class LoginUserFailure implements Action {
  public readonly type = AuthActionsTypes.LoginUserFailure;

  constructor(public payload: any) {
  }
}

export class LoginUserSuccess implements Action {
  public readonly type = AuthActionsTypes.LoginUserSuccess;

  constructor(public payload: any) {
  }
}

export class LoginRedirect implements Action {
  public readonly type = AuthActionsTypes.LoginRedirect;

  constructor(public payload: boolean) {
  }
}

export class LogoutUser implements Action {
  public readonly type = AuthActionsTypes.LogoutUser;
}

export type AuthActions =
  LoginUser
  | LoginUserSuccess
  | LoginUserFailure
  | LoginRedirect
  | LogoutUser
  | GetCurrentUserSuccess
  | GetCurrentUser;
