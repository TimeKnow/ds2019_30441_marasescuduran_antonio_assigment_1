import {Action} from '@ngrx/store';
import {Caregiver} from '../../core/models/caregiver';
import {Patient} from '../../core/models/patient';

export enum CaregiverActionsTypes {
  GetAllCaregivers = '[Caregiver] Get All',
  GetAllCaregiversSuccess = '[Caregiver] Get All Success',
  GetAllCaregiversFailure = '[Caregiver] Get All Failure',
  GetCaregiverById = '[Caregiver] Get By Id',
  GetCaregiverByIdSuccess = '[Caregiver] Get By Id Success',
  GetCaregiverByIdFailure = '[Caregiver] Get By Id Failure',
  DeleteCaregiver = '[Caregiver] Delete',
  DeleteCaregiverSuccess = '[Caregiver] Delete Success',
  DeleteCaregiverFailure = '[Caregiver] Delete Failure',
  UpdateCaregiver = '[Caregiver] Update',
  UpdateCaregiverSuccess = '[Caregiver] Update Success',
  UpdateCaregiverFailure = '[Caregiver] Update Failure',
  CreateCaregiver = '[Caregiver] Create',
  CreateCaregiverSuccess = '[Caregiver] Create Success',
  CreateCaregiverFailure = '[Caregiver] Create Failure',
  AttachPatientToCaregiver = '[Caregiver] Attach',
  AttachPatientToCaregiverSuccess = '[Caregiver] Attach Success',
  AttachPatientToCaregiverFailure = '[Caregiver] Attach Failure',
  GetAllPatientsOfCaregivers = '[Caregiver] Get All Patients Of',
  GetAllPatientsOfCaregiversSuccess = '[Caregiver] Get All Patients Of Success',
  GetAllPatientsOfCaregiversFailure = '[Caregiver] Get All Patients Of Failure',
}

export class GetAllCaregivers implements Action {
  public readonly type = CaregiverActionsTypes.GetAllCaregivers;

  constructor() {
  }
}

export class GetAllCaregiversSuccess implements Action {
  public readonly type = CaregiverActionsTypes.GetAllCaregiversSuccess;

  constructor(public payload: Caregiver[]) {
  }
}

export class GetAllCaregiversFailure implements Action {
  public readonly type = CaregiverActionsTypes.GetAllCaregiversFailure;

  constructor(public payload: any) {
  }
}

export class GetCaregiverById implements Action {
  public readonly type = CaregiverActionsTypes.GetCaregiverById;

  constructor(public payload: number) {
  }
}

export class GetCaregiverByIdSuccess implements Action {
  public readonly type = CaregiverActionsTypes.GetCaregiverByIdSuccess;

  constructor(public payload: Caregiver) {
  }
}

export class GetCaregiverByIdFailure implements Action {
  public readonly type = CaregiverActionsTypes.GetCaregiverByIdFailure;

  constructor(public payload: any) {
  }
}

export class DeleteCaregiver implements Action {
  public readonly type = CaregiverActionsTypes.DeleteCaregiver;

  constructor(public payload: number) {
  }
}

export class DeleteCaregiverSuccess implements Action {
  public readonly type = CaregiverActionsTypes.DeleteCaregiverSuccess;

}

export class DeleteCaregiverFailure implements Action {
  public readonly type = CaregiverActionsTypes.DeleteCaregiverFailure;

  constructor(public payload: any) {
  }
}

export class UpdateCaregiver implements Action {
  public readonly type = CaregiverActionsTypes.UpdateCaregiver;

  constructor(public id: number, public payload: Caregiver) {
  }
}

export class UpdateCaregiverSuccess implements Action {
  public readonly type = CaregiverActionsTypes.UpdateCaregiverSuccess;

  constructor(public id: number, public payload: Caregiver) {
  }

}

export class UpdateCaregiverFailure implements Action {
  public readonly type = CaregiverActionsTypes.UpdateCaregiverFailure;

  constructor(public payload: any) {
  }
}

export class CreateCaregiver implements Action {
  public readonly type = CaregiverActionsTypes.CreateCaregiver;

  constructor(public payload: Caregiver) {
  }
}

export class CreateCaregiverSuccess implements Action {
  public readonly type = CaregiverActionsTypes.CreateCaregiverSuccess;

  constructor(public payload: Caregiver) {
  }
}

export class CreateCaregiverFailure implements Action {
  public readonly type = CaregiverActionsTypes.CreateCaregiverFailure;

  constructor(public payload: any) {
  }
}

export class AttachPatientToCaregiver implements Action {
  public readonly type = CaregiverActionsTypes.AttachPatientToCaregiver;

  constructor(public caregiverId: number, public patientId: number) {
  }
}

export class AttachPatientToCaregiverSuccess implements Action {
  public readonly type = CaregiverActionsTypes.AttachPatientToCaregiverSuccess;

  constructor(public payload: Patient) {
  }
}

export class AttachPatientToCaregiverFailure implements Action {
  public readonly type = CaregiverActionsTypes.AttachPatientToCaregiverFailure;

  constructor(public payload: any) {
  }
}

export class GetAllPatientsOfCaregivers implements Action {
  public readonly type = CaregiverActionsTypes.GetAllPatientsOfCaregivers;

  constructor(public caregiverId: number) {
  }
}

export class GetAllPatientsOfCaregiversSuccess implements Action {
  public readonly type = CaregiverActionsTypes.GetAllPatientsOfCaregiversSuccess;

  constructor(public payload: Patient[]) {
  }
}

export class GetAllPatientsOfCaregiversFailure implements Action {
  public readonly type = CaregiverActionsTypes.GetAllPatientsOfCaregiversFailure;

  constructor(public payload: any) {
  }
}

export type CaregiverActions =
  GetAllCaregivers
  | GetAllCaregiversSuccess
  | GetAllCaregiversFailure
  | GetCaregiverById
  | GetCaregiverByIdSuccess
  | GetCaregiverByIdFailure
  | DeleteCaregiver
  | DeleteCaregiverSuccess
  | DeleteCaregiverFailure
  | UpdateCaregiver
  | UpdateCaregiverSuccess
  | UpdateCaregiverFailure
  | CreateCaregiver
  | CreateCaregiverSuccess
  | CreateCaregiverFailure
  | AttachPatientToCaregiver
  | AttachPatientToCaregiverSuccess
  | AttachPatientToCaregiverFailure
  | GetAllPatientsOfCaregivers
  | GetAllPatientsOfCaregiversSuccess
  | GetAllPatientsOfCaregiversFailure;
