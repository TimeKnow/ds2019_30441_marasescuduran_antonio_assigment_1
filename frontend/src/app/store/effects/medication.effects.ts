import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap, tap, timeout} from 'rxjs/operators';
import {of} from 'rxjs';
import {MedicationService} from '../../core/services/medication.service';
import {
  MedicationActionsTypes,
  CreateMedication,
  CreateMedicationFailure,
  CreateMedicationSuccess,
  DeleteMedication,
  DeleteMedicationFailure,
  DeleteMedicationSuccess,
  GetAllMedications,
  GetAllMedicationsFailure,
  GetAllMedicationsSuccess,
  UpdateMedication,
  UpdateMedicationFailure,
  UpdateMedicationSuccess,
  GetMedicationById,
  GetMedicationByIdSuccess,
  GetMedicationByIdFailure
} from '../actions/medication.actions';
import {Router} from '@angular/router';

@Injectable()
export class MedicationEffects {
  constructor(
    private medicationService: MedicationService,
    private actions$: Actions,
    private router: Router,
  ) {
  }

  @Effect()
  getAllMedications$ = this.actions$.pipe(
    ofType<GetAllMedications>(MedicationActionsTypes.GetAllMedications),
    switchMap(credential => this.medicationService.getAllMedications().pipe(
      timeout(5000),
      map(medications => new GetAllMedicationsSuccess(medications)),
      catchError(error => of(new GetAllMedicationsFailure(error)))
    ))
  );

  @Effect()
  getMedicationById$ = this.actions$.pipe(
    ofType<GetMedicationById>(MedicationActionsTypes.GetMedicationById),
    map(action => action.payload),
    switchMap(medicationId => this.medicationService.getMedicationById(medicationId).pipe(
      timeout(5000),
      map(medication => new GetMedicationByIdSuccess(medication)),
      catchError(error => of(new GetMedicationByIdFailure(error)))
    ))
  );

  @Effect()
  deleteMedication$ = this.actions$.pipe(
    ofType<DeleteMedication>(MedicationActionsTypes.DeleteMedication),
    map(action => action.payload),
    switchMap(id => this.medicationService.deleteMedication(id).pipe(
      timeout(3000),
      map(() => new DeleteMedicationSuccess()),
      catchError(error => of(new DeleteMedicationFailure(error)))
    ))
  );

  @Effect()
  updateMedication$ = this.actions$.pipe(
    ofType<UpdateMedication>(MedicationActionsTypes.UpdateMedication),
    switchMap(action => this.medicationService.updateMedication(action.id, action.payload).pipe(
      timeout(3000),
      map(updatedMedication => new UpdateMedicationSuccess(action.id, updatedMedication)),
      catchError(error => of(new UpdateMedicationFailure(error)))
    ))
  );

  @Effect({dispatch: false})
  updateMedicationSuccess$ = this.actions$.pipe(
    ofType<UpdateMedicationSuccess>(MedicationActionsTypes.UpdateMedicationSuccess),
    tap(() => this.router.navigateByUrl('/medications'))
  );

  @Effect()
  createMedication$ = this.actions$.pipe(
    ofType<CreateMedication>(MedicationActionsTypes.CreateMedication),
    map(action => action.payload),
    switchMap(newMedication => this.medicationService.createMedication(newMedication).pipe(
      timeout(3000),
      map(createdMedication => new CreateMedicationSuccess(createdMedication)),
      catchError(error => of(new CreateMedicationFailure(error)))
    ))
  );

  @Effect({dispatch: false})
  createMedicationSuccess$ = this.actions$.pipe(
    ofType<CreateMedicationSuccess>(MedicationActionsTypes.CreateMedicationSuccess),
    tap(() => this.router.navigateByUrl('/medications'))
  );
}
