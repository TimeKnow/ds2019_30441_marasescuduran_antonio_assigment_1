import {AppState} from '../state/app.state';
import {createSelector} from '@ngrx/store';
import {CaregiverState} from '../state/caregiver.state';

const selectCaregiverState = (state: AppState) => state.caregiverState;

export const selectCaregiverList = createSelector(
  selectCaregiverState,
  (state: CaregiverState) => state.caregiverList
);

export const selectedCaregiverStateCaregiverPatientList = createSelector(
  selectCaregiverState,
  (state: CaregiverState) => state.caregiverPatientList
);

export const selectedCaregiverStateSelectedCaregiver = createSelector(
  selectCaregiverState,
  (state: CaregiverState) => state.selectedCaregiver
);

export const selectCaregiverStateIsLoading = createSelector(
  selectCaregiverState,
  (state: CaregiverState) => state.isLoading
);

export const selectCaregiverStateErrorMessage = createSelector(
  selectCaregiverState,
  (state: CaregiverState) => state.errorMessage
);
